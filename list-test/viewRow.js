let MyRow = Backbone.View.extend({
    tagName: 'tr',
    events: {
        'clock #add': 'add',
        'click #del': 'del',
        'click #removeRow': 'remoweRow',
    },
    initialize() {
        this.template = _.template($('#tplBlock').html());
        this.listenTo(this.model, 'change', this.render);
        this.listenTo(this.model, 'destroy', this.remove);
        this.render()
    },
    render() {
        //debugger
        let view = this.template(this.model.toJSON())
        this.$el.html(view);
        return this
    },
    add() {
        debugger
        this.model.set({
            value: this.model.get('value') + 10
        })
    },
    del() {
        debugger
        this.model.set({
            value: this.model.get('value') - 10
        })
    },
    remoweRow() {
        this.model.destroy();
    },
});