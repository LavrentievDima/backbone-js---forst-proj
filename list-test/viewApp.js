let MyApp = Backbone.View.extend({
    events: {
        'click .addRow': 'addRow'
    },
    initialize() {
        this.template = _.template($('#appTpl').html());
        this.$el.html(this.template());
        this.coll = new MyCollection([{name: 'Tet', value: 10}, {name: 'Ret', value: 130}]);
        //this.listenTo(this.coll, 'all', this.render);
        this.listenTo(this.coll, 'add', this.addOne);
        this.render()
    },
    render() {
        console.log(this);
        this.coll.each(function(person) {
            const personView = new MyRow({model: person});
            this.$el.append(personView.render().el);
        }, this);

        return this;
    },
    addRow() {
        //debugger
        this.coll.add({})
    },
    addOne(model) {
        //
        let view = new MyRow({model: model})
        this.$('.rowList').append(view.render().el)

    }
})