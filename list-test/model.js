let MyModel = Backbone.Model.extend({
    defaults: {
        name: 'Trol',
        value: 0
    },
    validate({value}) {
        if(value < 0 || value > 200) return 'err'
    }
});

let MyCollection = Backbone.Collection.extend({
    model: MyModel,
    sortParam: 'size',
    sortMode: 1,
    comperator(a, b) {
        if(a.get(this.sortParam) > a.get(this.sortParam)) return -1 * this.sortMode;
        if(a.get(this.sortParam) < a.get(this.sortParam)) return this.sortMode;
        return 0
    }
})