const MyCollection = Backbone.Collection.extend({
    model: MyModel,
    sortParam: 'value',
    sortMode: 1,
    comparator(a, b) {
         if(a.get(this.sortParam) > b.get(this.sortParam)) return -1* this.sortMode
         if(a.get(this.sortParam) < b.get(this.sortParam)) return this.sortMode
        return 0
    }
});
