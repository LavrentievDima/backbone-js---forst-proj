let app = {};

$(function () {

    app.MyModel = Backbone.Model.extend({
        defaults: {
            name: 'Dima',
            description: '44 2243',
            size: 100
        },
        initialize: function () {
            console.log('obj is creaed');
            this.on('change', function () {
                console.log(app.MyModel.changedAttributes()); // только измененная часть
                console.log(app.MyModel.toJSON());
            })
        },
        validate(attrs) {
            if(attrs.size > 300) {
                console.log('Size > 300 ');
                return 'Size > 300'
            }
        },
        incraseSize() {
            app.MyModel.set({
                size: app.MyModel.get('size') + 20
            }, {
                validate: true,
            })
        }
    });
    app.MyModel = new app.MyModel({
        name: 'r',
        description: '123 123'
    })

    app.MyModel.set({
        size: 250,
    })

    $('#btn').on('click', () => app.MyModel.incraseSize());


    let tpl = _.template($('#tplBlock').html());
    $('#block').append(tpl({one: 1, two:22}))
});
