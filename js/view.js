let MyView = Backbone.View.extend({

    events: {
        'click #add': 'add',
        'click #remove': 'remove',
        'click .del': 'delRow'
    },
    initialize() {
        this.template = _.template($('#tplBlock').html());
        this.listenTo(this.model, "change", this.render);
        this.render();
    },

    render() {
        let json = this.model.toJSON();
        let view = this.template(json);
        this.$el.html(view);
        console.log(this.model.toJSON());
    },
    add() {
        this.model.set({
            'value': this.model.get('value') + 10
        }, {validate: true})
    },
    remove() {
        this.model.set({
            'value': this.model.get('value') - 10
        }, {validate: true})
    }

});