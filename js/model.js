let MyModel = Backbone.Model.extend({
    defaults: {
        name: 'Tro lo lo',
        value: 0
    },
    validate({value}) {
        if(value > 100 || value < 0) {
            return 'err size: ' + value;
        }
    }
})